# Arch Package Repository for Pd-L2Ork and Friends

**NOTE:** As of August 2019, I'm maintaining all these packages at the [Open Build Service](https://build.opensuse.org/) now. Please head over to https://build.opensuse.org/project/show/home:aggraef to find the new repositories.The following wiki page will tell you how to install from the OBS repositories: https://github.com/agraef/purr-data/wiki/Installation

This repository will stick around for the time being, but won't be updated any longer.

Permanent link to this page for bookmarking: https://l2orkaur.bitbucket.io

This repository provides a build of Pd-L2Ork and corresponding Pure and Faust plugins, as well as all required dependencies not readily available in the standard Arch repositories.

[Pd-L2Ork](http://l2ork.music.vt.edu/main/make-your-own-l2ork/software/) is the [Linux Laptop Orchestra](http://l2ork.music.vt.edu/) (L2Ork) version of [Pure Data](http://puredata.info/) (also known as Pd), [Miller Puckette's](http://msp.ucsd.edu/) real-time graphical programming environment for audio and graphics processing. Pd-L2Ork was created by [Ivica Ico Bukvic](http://www.performingarts.vt.edu/faculty-staff/view/ivica-ico-bukvic) from the School of Performing Arts at Virginia Tech. It is based on [Hans-Christoph Steiner's](http://at.or.at/hans/) popular (but no longer maintained) [Pd-extended](http://puredata.info/download/pd-extended) distribution of Pd, but includes many additional bugfixes and user interface enhancements.

The main package in the repository is Jonathan Wilkes' new [nw.js](http://nwjs.io/)-based Pd-L2Ork 2.x version, nick-named [Purr Data](https://agraef.github.io/purr-data/). This is a new cross-platform version of Pd-L2Ork with an improved JavaScript GUI.

For the time being, Bukvic's original [Pd-L2Ork](http://l2ork.music.vt.edu/main/make-your-own-l2ork/software/) (Pd-L2Ork 1.x a.k.a. "classic Pd-L2Ork") is still available and can be installed alongside the new 2.x version.

The repository also provides Pd-Pure and Pd-Faust plugins for use with Pd-L2Ork/Purr-Data. Using these plugins you can extend Pd-L2Ork with externals written in the [Pure](http://purelang.bitbucket.org/) and [Faust](http://faust.grame.fr/) programming languages.

All packages available here can also be installed directly from the [AUR](https://aur.archlinux.org/), please see the AUR links in the package list below. But it is usually more convenient and much quicker to install from the binary package repository. To these ends, add this entry to your `pacman.conf`:

```
[l2ork-aur]
SigLevel = Never
Server = https://l2orkaur.bitbucket.io/$arch
```

Note that installing pdl2ork-pure/pdl2ork-faust or purrdata-pure/purrdata-faust requires some dependencies from the Pure repository, so if you plan to use those packages then you'll also need to add this entry to your `pacman.conf`:

```
[pure-aur]
SigLevel = Never
Server = https://pureaur.bitbucket.io/$arch
```

Then run `sudo pacman -Sy` to make these repositories known to pacman, and use `sudo pacman -S` or your favorite Arch package manager to install the packages that you need.

If you don't want to change your `pacman.conf` then you can also [download](https://bitbucket.org/l2orkaur/l2orkaur.bitbucket.org/downloads) the repository or grab individual packages [here](https://bitbucket.org/l2orkaur/l2orkaur.bitbucket.org/src), and install the packages manually using, e.g., `sudo pacman -U`.

## EOL: 32 Bit Support

Please note that Arch and most of its derivatives don't support 32 bit systems any longer. As a result, while we still keep the 32 bit packages in this repository around for the time being, they won't be updated any longer. If you're still running a 32 bit Arch system, you should be able to install the latest version of these packages directly from the AUR instead, see below.

## Reporting Bugs

Out-of-date packages and smaller problems can be reported in the comment section of the AUR as usual, please follow the AUR links below for that. In addition, there is an [issue tracker](https://bitbucket.org/l2orkaur/l2orkaur.bitbucket.org/issues) on this page for more elaborate bug reports.

## AUR Packages

All packages in this repository are also available in the Arch User Repositories ([AUR](https://aur.archlinux.org/)), so using yay or your favorite AUR package manager you can also install them from there if you prefer that:

- https://aur.archlinux.org/packages/pd-l2ork/
- https://aur.archlinux.org/packages/purr-data/
- https://aur.archlinux.org/packages/pdl2ork-faust/
- https://aur.archlinux.org/packages/pdl2ork-pure/
- https://aur.archlinux.org/packages/purrdata-faust/
- https://aur.archlinux.org/packages/purrdata-pure/

Please note that the pd-l2ork and purr-data packages need a *long* time to build, so be patient or use the binary packages in this repository instead.

The packages above are stable snapshots which are updated from time to time when a new upstream release is out or if important bugfixes are available. Alternatively, you can also use the following packages which build directly from the latest upstream repository sources and are *only* available in the AUR:

- https://aur.archlinux.org/packages/pd-l2ork-git/
- https://aur.archlinux.org/packages/purr-data-git/

The following dependencies are also provided here since they aren't in the standard Arch repositories:

- https://aur.archlinux.org/packages/tkpng/
- https://aur.archlinux.org/packages/xapian-tcl-bindings/

## Alternative and Upstream Packages

- We also provide basically the same set of packages for Ubuntu, you can find these [here](https://l2orkubuntu.bitbucket.io/).

- Virginia Tech's official pd-l2ork packages are available at http://l2ork.music.vt.edu/main/make-your-own-l2ork/software/.

- Jonathan Wilkes' purr-data packages can be found at https://github.com/agraef/purr-data/releases (you can also find packages for OS X/macOS and Windows there).
